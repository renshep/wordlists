#!/bin/python3
import re, json
with open('src/2of12id.txt') as f:
    src_data = f.read().split('\n')[:-1]
nouns = []
verbs = []
adjs  = []
all_words = {}
for item in src_data:
    for word, pos, v in re.findall('^([\w\d]+)\s+(\w):(.*)', item):
        word = word.capitalize()
        variants = []
        if v:
            for va in re.split('\s+',v):
                for var in re.findall('^[\d\w]+',va):
                    variants.append(var.capitalize())
        all_words[word] = True
        for vw in variants:
            all_words[vw] = True
        if pos == 'V':
            verbs.append(word)
            verbs += variants
        elif pos == 'A':
            adjs.append(word)
            adjs += variants
        elif pos == 'N':
            nouns.append(word)
print('     Nouns: ' + str(len(nouns)))
print('     Verbs: ' + str(len(verbs)))
print('Adjectives: ' + str(len(verbs)))
print('       All: ' + str(len(all_words)))
def write_file(arr,file):
    with open(f'{file}.txt','w') as f:
        f.write('\n'.join(arr))
    file_json = json.dumps(arr)
    with open(f'{file}.json','w') as f:
        f.write(file_json)
    with open(f'{file}.js','w') as f:
        f.write(f'window.wordList_{file} = ' + file_json)
write_file(nouns,'nouns')
write_file(verbs,'verbs')
write_file(adjs,'adjs')
write_file(list(all_words),'all_words')

